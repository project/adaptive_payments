// $Id:

Welcome to adaptive_payments.
--------------------------
Instalation:
No special instructions are needed.

Use:
Please read the function headers for full description of each operations.
Read and understand the PayPal documents.
https://www.x.com/docs/DOC-1427<br>
https://www.x.com/servlet/JiveServlet/downloadBody/1531-102-3-2020/PP_AdaptivePayments.pdf

Use the included test module for testing and examples.

Notes:
* THIS MODULE IS NOT READY FOR PRODUCTION USE. It has been released to the
  community to help with testing and bug fixes.
*The API requires PayPal credentials to be set in a hook: hook_ap_presets;
*There is an additional App ID to your standard PayPal credentials
  that is required for Adaptive payments. For testing, you can obtain this ID
  by registering on the PayPal X site:
  http://www.x.com
  and click on My Apps.

